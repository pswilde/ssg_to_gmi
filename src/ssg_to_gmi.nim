import argparse
import convert
import strutils

var p = newParser:
  option("-i","--input", help="The directory containing your input files")
  option("-o","--output", help="The directory where the output files will be saved")
  option("-d","--depth", help="Sets how many levels the navigation will parse")

try:
  let opts = p.parse()
  when isMainModule:
    if opts.input != "" and
      opts.output != "":
      var depth = 0
      if opts.depth != "":
        depth = opts.depth.parseInt()
      convertDirectory(opts.input, opts.output, depth)
    else:
      echo "No Input or Output file provided"
      echo p.help
except ShortCircuit as err:
  if err.flag == "argparse_help":
    echo err.help
    quit(1)
except UsageError:
  stderr.writeLine getCurrentExceptionMsg()
  quit(1)

