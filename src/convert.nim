import os
import strutils
import re
import parsetoml
#import yaml
import json
import algorithm
import sequtils
import times

type
  Mode = enum
    TOML, YAML
  GmiFile = object
    title: string
    description: string
    draft: bool
    date: DateTime
    meta: Meta
    content: string
    url: seq[string]
    isSection: bool
  Meta = JsonNode
  Link = object
    href: string
    title: string
    description: string
    weight: int



proc getUrl(g: GmiFile): string =
  return g.url.join("/")

let toml_meta_re = re"[+]+.*[+]+"
let yaml_meta_re = re"[-]+.*[-]+"
proc tryParseMeta(str: string, mode: Mode): (bool,Meta) =
  if isEmptyOrWhitespace(str):
    return (false,Meta())
  try:
    case mode:
      of TOML:
        echo "Trying to Parse TOML"
        let toml = parseString(str)
        let meta = toml.toJson
        return (true, meta)
      of YAML:
        echo "YAML"
        echo "Not supported yet"
  except:
    echo "Failed to parse, it might not be meta..."
    return (false,Meta())
  return (false, Meta())

const DATETIME_FORMATS = [
                          "yyyy-MM-dd'T'HH:mm:sszzz",
                          "yyyy-MM-dd'T'HH:mm:ss'Z'"
                          ]
proc tryDateTimeParse(instr: string): (bool,DateTime) =
  let str = instr.strip(chars={'"'})
  for f in DATETIME_FORMATS:
    try:
      let d = (true,parse(str,f))
      echo "Parsed : ", str, " with ", f
      return d
    except:
      echo "Cannot parse date : ", str, " with ", f
      echo getCurrentExceptionMsg()
      continue
  return (false,DateTime())

let link_title_re = """\[[\w\s.:\/']+\]"""
let link_href_re = """\([^)]+\)"""
let link_re = re(link_title_re & link_href_re)
proc parseMarkdown(current_content: string): string =
  var content = current_content
  # Here we'll check through the content for links and then add them in a list
  # to the end of the content body
  var links: seq[Link]
  let matches = content.findAll(link_re)
  var c = 1
  for match in matches:
    echo "Found link : ", match
    let l = match.split("](")
    var link = Link()
    link.title = l[0].replace("[","")
    link.href = l[1].replace(")","")
    link.weight = c
    c += 1
    links.add(link)
    var link_handled = false
    for full_line in splitLines(content):
      let line = full_line.strip()
      if line != match:
        continue
      let link_mu = "=> " & link.href & " " & link.title & "\r"
      links.keepIf(proc(l: Link): bool = l.href != link.href)
      c -= 1
      content = content.replace(full_line,link_mu)
      link_handled = true
    if not link_handled:
      content = content.replace(match, link.title & "[" & $link.weight & "]")
  if len(links) > 0:
    var link_cont = "\n# Links\r\n"
    for link in links:
      link_cont &= "=> " & link.href & " " & $link.weight & ". " & link.title & "\r\n"
    content &= link_cont
  return content

proc parseContent(parts: seq[string], url: seq[string], mode: Mode): GmiFile =
  var gmi: GmiFile
  gmi.url = url
  gmi.isSection = url[^1] == "_index"
  for part in parts:
    if part == "":
      continue
    else:
      let (is_markup, meta) = tryParseMeta(part, mode)
      if is_markup:
        gmi.meta = meta
      else:
        gmi.content = parseMarkdown(part)
  let title = gmi.meta{"title"}{"value"}
  if title != nil:
    gmi.title = title.getStr
    let desc = gmi.meta{"description"}{"value"}
    if desc != nil:
      gmi.description = desc.getStr
    gmi.content = "## " & title.getStr() & "\r\n" & gmi.content
    if gmi.meta{"draft"}{"value"} != nil:
      gmi.draft = gmi.meta{"draft"}{"value"}.getStr.parseBool
    let dt = gmi.meta{"date"}{"value"}
    if dt != nil:
      let (ok,d) = tryDateTimeParse($dt)
      if ok:
        gmi.date = d
      else:
        gmi.date = now()
  return gmi

proc saveFile(out_file: string, content: GmiFile) =
  echo "Saving ", out_file
  let filename = extractFilename(out_file)
  let dir = out_file.replace(filename, "")
  if existsOrCreateDir(dir):
    writeFile(out_file,content.content)

proc convertFile(in_file, out_file, out_dir: string): GmiFile =
  let content = readFile(in_file)
  let url = out_file.replace(".gemini","").replace(out_dir, "").split("/")
  var output: GmiFile
  case content[0..2]:
    of "+++":
      echo "It's a TOML file... continuing..."
      output = parseContent(content.split(toml_meta_re), url, TOML)
    of "---":
      echo "It's a YAML file... Not Implemented yet."
      output = parseContent(content.split(yaml_meta_re), url, YAML)
    else:
      quit("Invalid TOML or YAML content")
  if output.draft == false:
    #.filter(proc(x: string): bool = x != "")
    saveFile(out_file, output)
    return output
  else:
    return GmiFile()

proc byWeight(x, y: Link): int =
  if x.weight > y.weight: 1
  else: -1

proc byDate(x, y: GmiFile): int =
  if x.date < y.date: 1
  else: -1

proc createSectionMenus(sections: seq[GmiFile], pages: seq[GmiFile], out_dir: string) =
  for section in sections:
    var links: seq[Link]
    echo "Creating Section Menus for ", section.title
    let s = section
    var ps = pages.filter(proc(page: GmiFile): bool = page.url[1] == s.url[1] and not page.draft)
    ps.sort(byDate)
    for p in ps:
      var link = Link()
      let url = p.getUrl.replace("_index","")
      link.href = url
      link.title = p.title
      link.description = p.description
      let weight = p.meta{"weight"}{"value"}
      if weight != nil:
        link.weight = weight.getStr().parseInt
      link.href = p.getUrl.replace("_index","")
      links.add(link)
    let filename = out_dir / section.getUrl & ".gemini"
    var section_menu = ""
    for link in links:
      section_menu &= "=> " & link.href & " " & link.title & "\r\n"
      section_menu &= link.description & "\r\n\r\n"
    if section_menu != "":
      let content = readFile(filename)
      writeFile(filename,content & "\r\n" & section_menu)


proc createFooterMenu(pages: seq[GmiFile], out_dir: string, myDepth: int = 1) =
  let depth = myDepth + 3
  echo depth
  echo "Creating footer links"
  var footer = "### Navigation\r\n"
  var links: seq[Link]
  for page in pages.filter(proc(x:GmiFile): bool = len(x.url) < depth or x.url[^1] == "_index"):
    let url = page.url.join("/").replace("_index","")
    var link = Link()
    link.href = url
    link.title = page.title
    let weight = page.meta{"weight"}{"value"}
    if weight != nil:
      link.weight = weight.getStr().parseInt
    link.href = page.url.join("/").replace("_index","")
    links.add(link)
  links.sort(byWeight)
  for link in links:
    echo "Found link : ", link
    footer &= "=> " & link.href & " " & link.title & "\r\n"
  if footer != "":
    echo "Writing footer to ", out_dir / "footer.gemini"
    writeFile(out_dir / "footer.gemini", footer)

proc checkAliases(gmi: GmiFile, out_dir: string) =
  if gmi.meta{"aliases"} != nil:
    for alias in gmi.meta{"aliases"}{"value"}:
      let redir = "31 " & gmi.getUrl & " "
      let file = out_dir / alias["value"].getStr & ".gemini"
      writeFile(file, redir)

proc convertDirectory*(in_dir: string, out_dir: string, depth: int = 1) =
  var pages: seq[GmiFile]
  var sections: seq[GmiFile]
  for in_file in walkDirRec(in_dir):
    var out_file = in_file.replace(in_dir,out_dir).replace(".md",".gemini")
    if in_file == in_dir & "/_index.md":
      out_file = out_file.replace("_index","index")
    echo "Parsing ", in_file, " in to ", out_file
    let gmi_file = convertFile(in_file, out_file, out_dir)
    if gmi_file.isSection:
      sections.add(gmi_file)
    elif gmi_file.content != "":
      pages.add(gmi_file)
    checkAliases(gmi_file, out_dir)
    echo "Completed Parsing ", in_file
  let out_files = sections.concat(pages)
  createSectionMenus(sections,pages, out_dir)
  createFooterMenu(out_files, out_dir, myDepth=depth)
  echo "Done. Your files should be in $#" % out_dir
